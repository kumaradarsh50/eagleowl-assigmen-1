import React from 'react';
import './TableContainer.css';
import DataTable from '../DataTAble/DataTable';
const tableContainer = () => {
  return (
    <div className='tab-container'>
      <ul className='tabs clearfix'>
        <li>
          <a href='/#'>disabled</a>
        </li>
        <li>
          <a href='/#'>untagged</a>
        </li>
        <li>
          <a href='/#'>incorrect</a>
        </li>
        <li className='active'>
          <a href='/#'>all recipe(s)</a>
        </li>
      </ul>
    </div>
  );
};

export default tableContainer;
