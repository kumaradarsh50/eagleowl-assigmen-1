import React from 'react';
import './Card.css';
import Progress from '../Progress/ProgressBar';

const card = () => {
  const state = {
    size: 80,
    progress: 40,
    strokeWidth: 5,
    circleOneStroke: '#F0EAE9',
    circleTwoStroke: '#3EA961',
    // #F0EAE9
    // #BCC7CF
    // #EB524F
  };
  return (
    <div className='card'>
      <div className='card--title'>high margin recipes</div>
      <div className='card__body'>
        <div className='card__body--item'>
          <div className='card__body--item--title'>ambur biryani</div>
          <div className='card__body--item--img'>
            <Progress {...state} />
          </div>
        </div>
        <div className='card__body--item'>
          <div className='card__body--item--title'>paneer tikka massala</div>
          <div className='card__body--item--img'>
            <Progress {...state} />
          </div>
        </div>
        <div className='card__body--item'>
          <div className='card__body--item--title'>ambur biryani</div>
          <div className='card__body--item--img'>
            <Progress {...state} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default card;
