import { render } from '@testing-library/react';
import React, { Component } from 'react';

class dataTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      isLoading: false,
      isError: false,
    };
  }

  //async function get reques
  async componentDidMount() {
    this.setState({ isLoading: true });
    const response = await fetch(
      'https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/recipes/?page=1'
    );

    if (response.ok) {
      const data = await response.json();
      const items = data.results;

      this.setState({ items, isLoading: false });
    } else {
      this.setState({ isError: true, isLoading: false });
    }
  }

  isIncorrectHandler = () => {
    return console.log('click');
  };

  renderTableHeader = () => {
    return (
      <tr>
        <th>
          <input type='checkbox' />
        </th>
        <th className='tablename'>name</th>
        <th className='tablename'>last update</th>
        <th>cogs%</th>
        <th>cost price(`)</th>
        <th>sale price</th>
        <th>gross margin</th>
        <th>tags / actions</th>
      </tr>
    );
  };

  renderTableRow = () => {
    return this.state.items.map((item) => {
      return (
        <tr key={item.id}>
          <td>
            <input type='checkbox' />
          </td>
          <td>{item.name}</td>
          <td>{item.last_updated.date.slice(0, 10)}</td>
          <td>{item.cogs}%</td>
          <td>{item.cost_price.toFixed(2)}</td>
          <td>{item.sale_price.toFixed(2)}</td>
          <td>{item.gross_margin.toFixed(0)}%</td>
          <td>
            <span className='span span--main'>indian</span>{' '}
            <span className='span span--sub'>indian menu</span>
          </td>
        </tr>
      );
    });
  };
  render() {
    const { items, isLoading, isError } = this.state;
    if (isLoading) {
      return <div>Loading...</div>;
    }
    if (isError) {
      return <div>Error loading</div>;
    }
    return items.length > 0 ? (
      <table>
        <thead>{this.renderTableHeader()}</thead>
        <tbody className='tablebody'>{this.renderTableRow()}</tbody>
      </table>
    ) : (
      <div>No item loading</div>
    );
  }
}
export default dataTable;
