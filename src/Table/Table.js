import React from 'react';
import './Table.css';
import TableData from '../TableData/TableData';

const table = (props) => {
  return (
    <div className='table'>
      <TableData />
    </div>
  );
};

export default table;
