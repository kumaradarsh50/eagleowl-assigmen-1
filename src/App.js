import React, { Component } from 'react';
import './App.css';

import Card from './Cards/Card';
import TableContainer from './TableContainer/TableContainer';
import Table from './Table/Table';

class App extends Component {
  render() {
    return (
      <div className='App'>
        <div className='container'>
          <Card />
          <Card />
          <Card />
        </div>
        <div className='tablecontainer'>
          <TableContainer />
        </div>
        <Table />
      </div>
    );
  }
}

export default App;
